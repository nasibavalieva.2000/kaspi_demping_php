<?php
require 'vendor/autoload.php';
include('db/index.php');
include('telegram/index.php');
include('google_services/getExcel.php');
include('scripts/getCookie.php');
include('scripts/getProductList.php');
include('scripts/getProductOffers.php');

$q = "SELECT * FROM test_users";
$users = mysqli_query($link, $q);

// foreach ($users as $user) {
//     $telegram->sendMessage([ 'chat_id' => $user['chat_id'], 'text' => 'TEST' ,'parse_mode'=>'HTML']);
// }

$products = getProductList();
$excelValues = getExcel();
$unioun_products = operation($products,$excelValues,true);

$links =array();
foreach ($unioun_products as &$product){
    array_push($links,
        array(
            "link"=>$product->masterProduct->productUrl,
            "name"=>$product->masterProduct->name
        )
    );
}

// $links = array(
//     array(
//         "link"=>'https://kaspi.kz/shop/p/luminarc-neo-carine-jacinthe-n8582-46-predmetov-100833011/',
//         "name"=>'Luminarc Neo Carine Jacinthe N8582 46 предметов'
//     ),
// 	array(
//         "link"=>'https://kaspi.kz/shop/p/vicalina-vl8801-10-predmetov-100702903/',
//         "name"=>'Vicalina VL8801 10 предметов'
//     ),
// 	array(
//         "link"=>'https://kaspi.kz/shop/p/force-41421r-142-predmeta-22700423/',
//         "name"=>'FORCE 41421R 142 предмета'
//     )
// );

print_r("Number of products(which is not processing right now): ".count($products)."\n");
print_r("Number of excelValues: ".count($excelValues)."\n");
print_r("Number of intersection products and excelValues: ".count($unioun_products)."\n");

getProductOffers($links,$unioun_products,$excelValues,$users,$telegram);

function operation($list1, $list2, $isUnion) {
    $result = array();
    
    for ($i = 0; $i <= count($list1); $i++) {
        if(isset($list1[$i])){
            $item1 = $list1[$i];
            $found = false;
    
            for ($j = 0; $j < count($list2) && !$found; $j++) {
                $found = $item1->masterProduct->name === $list2[$j][0];
            }
            if ($found === !!$isUnion) { // isUnion is coerced to boolean
                array_push($result,$item1);
            }
        }
    }
    return $result;
}

?>