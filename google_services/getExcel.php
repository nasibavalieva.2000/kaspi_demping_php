<?php

function getExcel(){
    $client = new \Google_Client();
    $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
    $client->setAccessType('offline');
    $client->setAuthConfig(__DIR__.'/credentials.json');
    $service = new Google_Service_Sheets($client);

    $spreadsheetId = "1_agep7clDCft8vgi_of-G2buupMfR9V--oOdfodQWQY"; 
    $get_range = "Себестоимость товаров!A:Z";
    $response = $service->spreadsheets_values->get($spreadsheetId, $get_range);
    $values = $response->getValues();

    $excelValues=array();
    foreach ($values as &$value) {
        if(isset($value[0]) && isset($value[6])){
            $name = $value[2];
            $price = preg_replace("/[^0-9]/", "",$value[10]);
            $sku = $value[0];
            $isChangable = $value[6];
            
            if($isChangable==='1'){
                array_push($excelValues,array($name,$price,$sku));
            }
        }
        
       
    }
    return $excelValues;

}

// getExcel();


?>
