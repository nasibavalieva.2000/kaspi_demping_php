<?php

function getProductList(){
    $url = 'https://kaspi.kz/merchantcabinet/api/offer';
    $post_data = '{
        "searchTerm":null,
        "offerStatus":"ACTIVE",
        "categoryCode":null,
        "cityId":null,
        "start":0,
        "count":1000
    }';

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.87 Safari/537.36');

    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type:application/json',
        'Content-Length:'.strlen($post_data)
    ));

    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

    //сохранять полученные COOKIE в файл
    // curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt');
    curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie.txt');

    $result=json_decode(curl_exec($ch));

    $array = array_filter($result->offers, function($obj) {
        if ($obj->nextGen === null) return true;
    });

    curl_close($ch);
    
    return $array;   
}

 
?>