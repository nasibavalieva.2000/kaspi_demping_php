<?php
function getProductOffers($links,$products,$excelValues,$users,$telegram){
    $cookie = 'ticket=TGT-2a6ea507-0c08-4907-84f8-665861df7163;';
    $headers   = array();
    $headers[] = 'Cookie: ' . $cookie;

    
    foreach ($links as &$link) {
        $new_price = null;
        print_r($link['link']."\n");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.87 Safari/537.36');
        curl_setopt($ch, CURLOPT_URL, $link['link']."offers/?c=750000000&limit=50&page=0&sort=asc");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        $out = curl_exec($ch);
        $curl_info = curl_getinfo($ch);
       
        if ($out === FALSE && $out == '' && $curl_info['http_code'] != 200) {
            print_r("Something went wrong\n");
            continue;
        }

        try {
            $res = json_decode($out);
            $merchants = $res->data;

            $minimum_price = $merchants[0]->unitSalePrice;
            $product_name = $link['name'];        
            $ind = searchForIndex('Letostore-kz',$merchants);
            $letostore_price = $merchants[$ind]->unitSalePrice;
            print_r("На сайте: ".$letostore_price."\n");
            $product_from_excel = searchForName($product_name, $excelValues);
            print_r($product_name."\n");

            if(isset($product_from_excel) && isset($ind)){
                $sebest =  $product_from_excel[1];
                $sku = $product_from_excel[2];
                $marzha = ($sebest>50000) ? ceil($sebest * 0.1) : ceil($sebest * 0.15);
                // print_r("Себестоимость: ".$sebest ."\n");
                // print_r("Маржа: ".$marzha ."\n");
                // print_r("Себестоимость+Маржа: ".(intval($sebest)+intval($marzha))."\n");
                
                //проверям меньше ли наша цена чем допустимая и меняем ее на допустимую+10%/15%
                if($letostore_price<intval($sebest)+intval($marzha)){
                    print_r("if1\n");
                    $new_price = $sebest + $marzha;
                    $is_bigger = true;
                    $comment = '❗️❗️❗️У этого товара маржа была меньше нужной и я увеличил его цену. Найдите поставщика подешевле ❗️❗️❗️';
                }
                //проверям больше ли наша цена чем допустимая 
                else{
                    //если мы не на первом месте
                    if($merchants[0]->name !== 'Letostore-kz' && $minimum_price <= $letostore_price){
                        $minimum_minus_one = (intval($minimum_price)-1);
                        //если допустимая цена+маржа меньше чем минимальная минус один
                        if($minimum_minus_one > $sebest+$marzha){
                            print_r("if2\n");
                            $new_price = $minimum_minus_one; 
                            $is_bigger = false;
                            $comment = 'Спустил цену на один тенге';
                        }else{
                            print_r("На первом месте чел с ценой ниже которой я не могу поставить. Надо найти другого поставщика.");
                        }
                    }
                    //если мы на первом месте, чтобы увеличивать цену 
                    else{
                        print_r("Мы на первом месте\n");
                        //если есть второе место и их цена минус один больше чем наша, подстраиваться под него 
                        if(isset($merchants[1])){
                            if(intval($merchants[1]->unitSalePrice)-1>$letostore_price){
                                $new_price = intval($merchants[1]->unitSalePrice)-1;
                                $is_bigger = true;
                                $comment = 'Поднял цену, чтобы подстроиться под второе место';
                                print_r("if3\n");
                            }
                        }
                        //если мы одни продаем этот товар то увеличивать маржу на 30%
                        else if(!isset($merchants[1]) && $letostore_price < ceil($sebest+($sebest * 0.3))){
                            $new_price = ceil($sebest+($sebest * 0.3));
                            $is_bigger = true;
                            $comment = 'Только мы продаем этот товар, я увеличил маржу на 30%';
                            print_r("if4\n");
                        }
                    }

                }

                if(isset($new_price)){
                    $merchants[$ind]->unitSalePrice = $new_price;
                    usort($merchants, "cmp");
                    $top5 = (count($merchants)>5) ? array_slice($merchants, 0, 5) : $merchants;
                    $top5String = "\n";
                    foreach($top5 as $key => $value){
                        $top5String.="<b>".($key+1).")</b><i>".$value->name.": ".$value->unitSalePrice." тг</i>\n";
                    }

                    $change_value =array(
                        'product_name'=>$product_name,
                        'allowable_price'=>$sebest+$marzha,
                        'new_price'=>$new_price,
                        'was_price'=>$minimum_price,
                        'top5'=>$top5String,
                        'is_bigger'=>$is_bigger,
                        'product_url'=>$link['link'],
                        'sku'=>$sku,
                        'comment'=>$comment
                    ); 

                    $emoji = $is_bigger ? '⬆' : '⬇';
                    $found_product = find($product_name,$products);
                    $cityData = array();
                    foreach ($found_product->cityInfo as &$city){
                        $new_city = $city;
                        $new_city->priceRow->price = $new_price;
                        array_push($cityData,$new_city);
                    }

                    $post_data = array(
                        'productSku'=>$sku,
                        'productName'=>$product_name,
                        'productBrand'=>$found_product->brand,
                        'cityData'=>$cityData,
                        'force'=>"false",
                        'new'=>"false"
                    );

                    print_r($change_value['new_price']." ".$emoji."\n");
                    //send to tg
                    foreach ($users as $user) {
                        $reply = 
                        "<b>Название товара: </b> <i>".$change_value['product_name']."</i>".
                        "\n<b>Самая низкая цена была: </b> <i>".$change_value['was_price']." тг.</i>".
                        "\n<b>Самая низкая цена стала: </b> <i>".$change_value['new_price']." тг.</i>".$emoji.
                        "\n<b>Наша допустимая цена(с маржой): </b> <i>".$change_value['allowable_price']." тг.</i>".
                        "\n<b>Ссылка на продукт: </b> <i>".$change_value['product_url']."</i>".
                        "\n<b>Топ 5: </b> <i>".$change_value['top5']."</i>".
                        "\n<b>Комментарий: </b> <i> ".$change_value['comment']."</i>";

                        $telegram->sendMessage([ 'chat_id' => $user['chat_id'], 'text' => $reply ,'parse_mode'=>'HTML']);
                    }

                    $url = 'https://kaspi.kz/merchantcabinet/api/offer/save';
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.87 Safari/537.36');
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type:application/json',
                        'Content-Length:'.strlen(json_encode($post_data))
                    ));
                    curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($post_data));
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                    curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie.txt');
                    $result=json_decode(curl_exec($ch));
                    curl_close($ch);

                    print_r($result);  
                    
                    sleep(3);

                }
            
            }else{
                print_r("Не нашел в таблице\n");
            }  
        } catch (Exception $e) {
            echo 'Поймано исключение: ',  $e->getMessage(), "\n";
            continue;
        } finally {
            echo "Первый блок finally.\n";
            curl_close($ch);
        }
    } 
    


}

function searchForName($name, $array) {
    foreach ($array as $key => $val) {
        if ($val[0] === $name) {
            return $val;
        }
    }
    return null;
}
function searchForIndex($name, $array) {
    foreach ($array as $key => $val) {
        if ($val->name === $name) {
            return $key;
        }
    }
    return null;
}
function cmp($a, $b) {
    return strcmp($a->unitSalePrice, $b->unitSalePrice);
}
function find($name, $array) {
    foreach ($array as $key => $val) {
        if ($val->masterProduct->name === $name) {
            return $val;
        }
    }
    return null;
}

?>